CXXFLAGS = -Wall -Wextra -pedantic -O -std=c++11
CXX = g++

bin: main
hello:
	echo "Hello Class!"
main: main.cpp
	$(CXX) $(CXXFLAGS) -o main main.cpp

clean:
	rm -rf *.o *~
