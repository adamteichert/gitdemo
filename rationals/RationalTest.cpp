#include <cassert>
#include "Rational.h"

int main() {
    Rational a(3, 4);
    Rational b(6, 8);

    // testing equality operator
    assert(a.getNumer() == 3);
    assert(a.getDenom() == 4);
    assert(a == b);
    Rational d(7, -5);
    Rational e(-7, 5);
    assert(d == e);
    assert(a.operator==(b));

    // test gcd
    assert(Rational::gcd(5, 30) == 5);
    assert(Rational::gcd(25, 35) == 5);
    assert(b.getNumer() == 3);
    assert(b.getDenom() == 4);

    // testing assignment operator
    Rational c(7, 5);
    b = c;
    assert(b.getNumer() == 7);
    assert(b.getDenom() == 5);

    // test copy constructor
    Rational g(c);
    assert(g.getNumer() == 7);
    assert(g.getDenom() == 5);
    return 0;
    
    // test addition op
    Rational f = a + b;
    assert(f.getNumer() == 3);
    assert(f.getDenom() == 2);

    // test todouble
    assert((double)f == 1.5);

    // test conversion constructor
    Rational h = 1;
    assert(h.getNumer() == 1);
    assert(h.getDenom() == 1);

    // test +=
    f += 1;
    assert((double)f == 2.5);
    
}
