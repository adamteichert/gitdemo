#include "Rational.h"

void Rational::reduce() {
    int d = gcd(numer, denom);
    numer /= d;
    denom /= d;
}
