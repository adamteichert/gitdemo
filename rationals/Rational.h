#ifndef MY_RATIONAL_H
#define MY_RATIONAL_H

class Rational {
    int numer;
    int denom;
public:
    Rational(const Rational& other): numer(other.numer), denom(other.denom) { }
    Rational(int inNumer=0, int inDenom=1): numer(inNumer), denom(inDenom) {
        if (denom < 0) {
            numer *= -1;
            denom *= -1;
        }
        reduce();
    }
    static int gcd(int a, int b) {
        if (a < 0) return gcd(-a, b);
        else if (b < 0) return gcd(a, -b);
        else if (a == b) return a;
        else if (a > b) return gcd(a - b, b);
        else return gcd(a, b - a);
    }
    Rational operator+(const Rational& rhs) const {
        return Rational(numer * rhs.denom + rhs.numer * denom,
                        denom * rhs.denom);
    }
    operator double() const {
        return (double) numer / denom;
    }
    void reduce();
    int getNumer() const { return numer; }
    int getDenom() const { return denom; }
    Rational& operator=(const Rational& rhs) {
        if (&rhs != this) {
            numer = rhs.numer;
            denom = rhs.denom;
        }
        return *this;
    }
    Rational& operator+=(const Rational& rhs) {
        return *this = *this + rhs;
    }

    bool operator==(const Rational& rhs) const {
        return numer == rhs.numer && denom == rhs.denom;
    }
    
};

#endif
